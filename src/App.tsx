import React from "react";

import GenreSelector from "./components/GenreSelector";
import Canvas from "./components/Canvas";

function App() {
  const [genre, setGenre] = React.useState("chillhop");
  const [text, setText] = React.useState("Nvlf");
  return (
    <div className="App text-center">
      <div className="m-2">
        <GenreSelector
          name="genre"
          value="chillhop"
          label="ChillHop"
          checked={genre === "chillhop"}
          setGenre={setGenre}
        />
        <GenreSelector
          name="genre"
          value="trap"
          label="Trap"
          checked={genre === "trap"}
          setGenre={setGenre}
        />
        <GenreSelector
          name="genre"
          value="synthwave"
          label="Synthwave"
          checked={genre === "synthwave"}
          setGenre={setGenre}
        />
      </div>
      <textarea
        rows={4}
        cols={48}
        className="p-2 rounded bg-gray-300"
        value={text}
        onChange={(e) => {
          setText(e.target.value);
        }}
      ></textarea>

      <Canvas
        width={50}
        height={50}
        text={text}
        genre={genre}
        title="Small circle"
        className="rounded-full"
      />

      <h2>
        <i className="uil uil-circle"></i> Spotify
        <Canvas
          width={2660}
          height={1440}
          text={text}
          genre={genre}
          title="Spotify Banner"
        />
        <Canvas
          width={750}
          height={750}
          text={text}
          genre={genre}
          title="Spotify Avatar"
        />
        <Canvas
          width={3000}
          height={3000}
          text={text}
          genre={genre}
          title="Spotify/Routenote Cover"
        />
      </h2>
      <h2>
        <i className="uil uil-circle"></i> SoundCloud
        <Canvas
          width={2480}
          height={520}
          text={text}
          genre={genre}
          title="SoundCloud Banner"
        />
        <Canvas
          width={1000}
          height={1000}
          text={text}
          genre={genre}
          title="SoundCloud Avatar"
        />
        <Canvas
          width={800}
          height={800}
          text={text}
          genre={genre}
          title="SoundCloud Cover"
        />
      </h2>
      <h2>
        <i className="uil uil-facebook-f"></i> Facebook
        <Canvas
          width={1640}
          height={856}
          text={text}
          genre={genre}
          title="Facebook Banner"
        />
        <Canvas
          width={180}
          height={180}
          text={text}
          genre={genre}
          title="Facebook Avatar"
        />
      </h2>
      <h2>
        <i className="uil uil-twitter-alt"></i> Twitter
        <Canvas
          width={1500}
          height={500}
          text={text}
          genre={genre}
          title="Twitter Banner"
        />
        <Canvas
          width={400}
          height={400}
          text={text}
          genre={genre}
          title="Twitter Avatar"
        />
      </h2>
      <h2>
        <i className="uil uil-instagram"></i> Instagram
        <ul>
          <Canvas
            width={180}
            height={180}
            text={text}
            genre={genre}
            title="Instagram Avatar"
          />
          <Canvas
            width={1080}
            height={1080}
            text={text}
            genre={genre}
            title="Instagram Photo Square"
          />
          <Canvas
            width={1080}
            height={1350}
            text={text}
            genre={genre}
            title="Instagram Photo Portrait"
          />
          <Canvas
            width={1080}
            height={608}
            text={text}
            genre={genre}
            title="Instagram Photo Landscape"
          />
          <Canvas
            width={600}
            height={600}
            text={text}
            genre={genre}
            title="Instagram Video Square"
          />
          <Canvas
            width={600}
            height={750}
            text={text}
            genre={genre}
            title="Instagram Video Portrait"
          />
          <Canvas
            width={600}
            height={315}
            text={text}
            genre={genre}
            title="Instagram Video Landscape"
          />
          <Canvas
            width={600}
            height={600}
            text={text}
            genre={genre}
            title="Instagram Video Carousel"
          />
          <Canvas
            width={1080}
            height={1920}
            text={text}
            genre={genre}
            title="Instagram Stories Photo"
          />
          <Canvas
            width={420}
            height={654}
            text={text}
            genre={genre}
            title="Instagram IGTV Cover"
          />
        </ul>
      </h2>
      <h2>
        <i className="uil uil-circle"></i> TikTok
        <Canvas
          width={200}
          height={200}
          text={text}
          genre={genre}
          title="TikTok Avatar"
        />
        <Canvas
          width={1080}
          height={1920}
          text={text}
          genre={genre}
          title="TikTok Video"
        />
      </h2>
    </div>
  );
}

export default App;
