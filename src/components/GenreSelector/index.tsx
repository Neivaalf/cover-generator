import React from "react";

function GenreSelector({
  name,
  value,
  label,
  setGenre,
  checked,
}: {
  name: string;
  value: string;
  label: string;
  setGenre: React.Dispatch<React.SetStateAction<string>>;
  checked?: boolean;
}) {
  const getCustomStyle = () => {
    if (value === "chillhop")
      return {
        fontFamily: "CrimsonFoam",
        color: checked ? "#44bba4" : undefined,
      };
    if (value === "trap")
      return {
        fontFamily: "KingthingsTrypewriter2",
        color: checked ? "#437c90" : undefined,
      };
    if (value === "synthwave")
      return {
        fontFamily: "DeadStock",
        color: checked ? "#db5461" : undefined,
      };
    return undefined;
  };

  return (
    <label
      htmlFor={value}
      className="p-2 text-4xl text-gray-300 cursor-pointer select-none hover:text-gray-400 transition-all duration-200 ease-in-out"
      style={getCustomStyle()}
    >
      <input
        type="radio"
        name={name}
        value={value}
        id={value}
        checked={checked}
        onChange={() => {
          setGenre(value);
        }}
        className="hidden"
      />
      {label}
    </label>
  );
}

export default GenreSelector;
