import React from "react";

function Canvas({
  height,
  width,
  text,
  genre,
  title,
  className,
}: {
  height: number;
  width: number;
  text: string;
  genre: string;
  title: string;
  className?: string;
}) {
  const MINT_CREAM = "#f7fff7";

  const KEPPEL = "#44bba4";
  const INDIAN_RED = "#db5461";
  const TEAL_BLUE = "#437c90";

  const canvasRef = React.useRef<HTMLCanvasElement>(null);

  React.useEffect(() => {
    const drawBackground = (ctx: CanvasRenderingContext2D) => {
      ctx.fillStyle = MINT_CREAM;
      ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    };

    const drawText = (ctx: CanvasRenderingContext2D) => {
      ctx.textAlign = "center";
      ctx.textBaseline = "middle";

      let fontSize = 0;
      let font = "";

      if (genre === "chillhop") {
        ctx.fillStyle = KEPPEL;

        fontSize = (192 * ctx.canvas.width) / 500;
        font = "CrimsonFoam";
      }
      if (genre === "trap") {
        ctx.fillStyle = TEAL_BLUE;

        fontSize = (192 * ctx.canvas.width) / 1100;
        font = "KingthingsTrypewriter2";
      }
      if (genre === "synthwave") {
        ctx.fillStyle = INDIAN_RED;

        fontSize = (192 * ctx.canvas.width) / 900;
        font = "DeadStock";
      }

      if (text.toLowerCase() === "nvlf") {
        fontSize *= 1.6;

        console.log("Hello", fontSize);
      }

      ctx.font = `${fontSize}px ${font}`;

      const lines = text.split("\n");

      const halfLength = Math.floor(lines.length / 2);

      lines.forEach((line, i) => {
        const offset =
          (i - halfLength) * fontSize +
          (lines.length % 2 === 0 ? fontSize / 2 : 0);

        ctx.fillText(
          line,
          ctx.canvas.width / 2,
          ctx.canvas.height / 2 + offset
        );
      });
    };

    // Get ctx
    if (!canvasRef.current) return;
    const ctx = canvasRef.current.getContext("2d");
    if (!ctx) return;

    setTimeout(() => {
      // Background
      drawBackground(ctx);

      // Text
      drawText(ctx);
    }, 250);
  }, [canvasRef, genre, text]);

  return (
    <div className="flex flex-col items-center mb-4">
      <details>
        <summary>{`${text} - ${title} (${width}x${height}) - ${genre}`}</summary>
        <canvas
          ref={canvasRef}
          height={height}
          width={width}
          className={`border-gray-300 border-2 mx-auto ${className ?? ""}`}
        ></canvas>
        <a
          className="px-2 py-1 bg-gray-300 hover:bg-gray-400 text-xs rounded-b transition-all duration-200 ease-in-out"
          href="#download"
          onClick={(e) => {
            if (!canvasRef.current) return;
            let dataURL;
            if (title === "Spotify/Routenote Cover") {
              dataURL = canvasRef.current.toDataURL("image/jpeg");
            } else {
              dataURL = canvasRef.current.toDataURL("image/png");
            }
            e.currentTarget.href = dataURL;
          }}
          download={
            title === "Spotify/Routenote Cover"
              ? `${text} - ${title} (${width}x${height}) - ${genre}.jpg`
              : `${text} - ${title} (${width}x${height}) - ${genre}.png`
          }
        >
          {`${text} - ${title} (${width}x${height}) - ${genre}`}{" "}
          <i className="uil uil-image-download text-base"></i>
        </a>
      </details>
    </div>
  );
}

export default Canvas;
